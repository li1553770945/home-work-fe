# homeworkfe3

![](https://img.shields.io/badge/vue-v3.2.0-brightgreen)  ![](https://img.shields.io/badge/homework-foreend-blue)  ![](https://img.shields.io/badge/style-bigBlur-yellow)

## 简介

使用绚丽的bigBlur风格设计的班级作业系统前端，个人感觉还算凑合的一次设计。当然，体验也不错，幸好今天只有一节课，所以只花了一天就把peacesheep大佬之前的作业系统的前端部分重构完了。首页预览：

![](https://cdn.jsdelivr.net/gh/LSTM-Kirigaya/KImage/Img/20220411160225.png)

## TODO

- [ ] 更好的移动端体验（~~因为手机浏览没有支持~~）
- [ ] 支持用户上传图片
- [ ] K的小工具



## Feature

- 自由调节颜色：你可以通过首页的调色板设置全局的颜色。整个系统的颜色通过:root中的--mainColor控制，其中进度条的颜色通过HSV空间的V值线性采样得到cmap，别的亮色和阴影色也具有简单的运算逻辑。

![](https://cdn.jsdelivr.net/gh/LSTM-Kirigaya/KImage/Img/20220411160338.png)


- 可选择的背景图片：未来还会支持直接从本地读取base64

![](https://cdn.jsdelivr.net/gh/LSTM-Kirigaya/KImage/Img/20220317232553.png)

- 高斯遮罩层：一般的对话框的遮罩层都是暗色的rgba，不过我喜欢bigBlur式的设计风格，所以遮罩层做成了高斯模糊，在部分手机浏览器（夸克，百度）上可能会失效

![](https://cdn.jsdelivr.net/gh/LSTM-Kirigaya/KImage/Img/image-20220317232836326.png)

- 完整的表单系统，如果你喜欢每个控件都具有统一色调的样式，或许你会感兴趣？

![](https://cdn.jsdelivr.net/gh/LSTM-Kirigaya/KImage/Img/XX{H$YO8N(TAGB4UZ)07]_K.jpg)
