const isProduction = process.env.NODE_ENV !== 'development';
const CompressionWebpackPlugin = require('compression-webpack-plugin')

module.exports = {
    lintOnSave : false,
    devServer: {
        proxy : {
            "/api" : {
                target : "https://1.13.158.63"
                // target : "http://localhost:8001"
            }
        }
    },
    publicPath : "/",
    productionSourceMap: false,
    css : {
        sourceMap : false
    },
    configureWebpack: config => {
        if (isProduction) {
            const productionGzipExtensions = ['html', 'js', 'css']
            config.plugins.push(
                new CompressionWebpackPlugin({
                    filename: '[path].gz[query]',
                    algorithm: 'gzip',
                    test: new RegExp(
                        '\\.(' + productionGzipExtensions.join('|') + ')$'
                    ),
                    threshold: 10240, 
                    minRatio: 0.8, 
                    deleteOriginalAssets: false 
                })
            )
        }
    },
    chainWebpack: config => {
        let configpkg
        if (isProduction)
            configpkg = {
                vue: 'Vue',
                vuex : 'Vuex',
                'vue-router': 'VueRouter',
                'axios':'axios',
                'element-plus': 'ElementPlus',
            }
        else {
            configpkg = {}
        }
        config.set('externals', configpkg)
    }
}