import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import "nprogress/nprogress.css"

import router from "@/router"
import store  from "@/store"

import mainImg from "@/components/mainImg"
import KTitle from "@/components/Writing/Title.vue"
import KQuote from "@/components/Writing/Quote.vue"
import KTitle3 from "@/components/Writing/DyTitle3.vue"
import KTag from "@/components/Writing/Tag.vue"
import KCard from "@/components/Writing/KCard.vue"
import KBlock from "@/components/Writing/KBlock.vue"
import KPagination from "@/components/Writing/KPagination.vue"
import KDialog from "@/components/Dialog"
import KFileInput from "@/components/Writing/KFileInput.vue"

createApp(App)
    .component("mainImg", mainImg)
    .component("KTitle", KTitle)
    .component("KQuote", KQuote)
    .component("KTitle3", KTitle3)
    .component("KTag", KTag)
    .component("KCard", KCard)
    .component("KBlock", KBlock)
    .component("KPagination", KPagination)
    .component("KDialog", KDialog)
    .component("KFileInput", KFileInput)
    .use(ElementPlus)
    .use(router)
    .use(store)
    .mount('#app')