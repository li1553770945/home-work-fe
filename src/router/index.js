import { createRouter, createWebHistory } from "vue-router"

const history = createWebHistory()
const router = createRouter({
    history,
    routes: [
        {
            path: "/:catchAll(.*)",
            redirect: "/home"
        },
        {
            path : "/home",
            component: () => import("@/pages/Home")
        },
        {
            path: '/me',
            name: 'Me',
            component: () => import("@/pages/Me"),
            meta: { title: "我" }
        },
        {
            path: '/new',
            name: 'New',
            component: () => import("@/pages/New"),
            meta: { title: "新建作业" }
        },
        {
            path: '/details',
            name: 'Details',
            component: () => import("@/pages/Details"),
            meta: { title: "作业详情" }
        },
        {
            path: '/myhomework',
            name: 'MyHomeWork',
            component: () => import("@/pages/MyHomeWork"),
            meta: { title: "我的作业" }
        },
        {
            path: '/myhomeworkcreate',
            name: 'MyHomeWorkCreate',
            component: () => import("@/pages/MyHomeWorkCreate"),
            meta: { title: "我发布的作业" }
        },
        {
            path: '/submit',
            name: 'Submit',
            component: () => import("@/pages/Submit"),
            meta: { title: "提交作业" }

        },
        {
            path: '/newgroup',
            name: 'NewGroup',
            component: () => import("@/pages/NewGroup"),
            meta: { title: "创建小组" }
        },
        {
            path: '/groupdetails',
            name: 'GroupDetails',
            component: () => import("@/pages/GroupDetails"),
            meta: { title: "组织信息" }
        },
        {
            path: '/groupcreate',
            name: 'GroupCreate',
            component: () => import("@/pages/GroupCreate"),
            meta: { title: "我创建的组织" }
        },
        {
            path: '/groupjoin',
            name: 'GroupJoin',
            component: () => import("@/pages/GroupJoin"),
            meta: { title: "我加入的组织" }
        },
        {
            path: '/groupmembers',
            name: 'GroupMembers',
            component: () => import("@/pages/GroupMembers"),
            meta: { title: "成员管理" }
        },
        {
            path: '/myhomeworknotdone',
            name: 'MyHomeWorkNotDone',
            component: () => import("@/pages/MyHomeWorkNotDone"),
            meta: { title: "未完成的作业" }
        },
        {
            path: '/export',
            name: 'Export',
            component: () => import("@/pages/Export"),
            meta: { title: "导出" }
        },
    ]
})

export default router