import r from "@/api/request"

// login 登录
export const reqLogin = (username, password) => r({
    url: "/login", method: "POST",
    data: { username, password }
})

// user 注册
export const reqEnroll = (username, password, name) => r({
    url: "/user", method: "POST",
    data: { username, password, name }
})

// user 查询
export const reqQueryInfo = () => r({
    url: "/user", method: "GET"
})

// logout 登出
export const reqLogout = () => r({
    url: "/logout", method: "GET"
})

// group 增加小组
export const reqAddGroup = (name, password, desc, allowCreate) => r({
    url: "/group", method: "POST",
    data: { name, password, desc, allowCreate }
})

//group 删除小组
export const reqDelGroup = (groupID) => r({
    url: "/group", method : "DELETE",
    data : {groupID}
})

// group 查询小组
export const reqQueryGroup = (groupID) => r({
    url: "/group", method : "GET",
    params : {groupID}
})

// group 修改小组
export const reqAdjustGroup = (groupID, name, password, desc, allowCreate) => r({
    url: "/group?groupID=" + groupID, method : "PUT",
    data: { name, password, desc, allowCreate }
})

// group-created-num 查询自己创建小组的数量
export const reqGroupCreateNum = () => r({
    url: "/group-created-num", method: "GET"
})

// group-created 查询自己创建的小组
export const reqGroupCreated = (start, end) => r({
    url: "/group-created", method: "GET",
    params : {start, end}
})

// group-member 加入小组
export const reqJoinGroup = (groupID, password) => r({
    url: "/group-member", method: "POST",
    data : {groupID, password}
})

// group-member 删除小组
export const reqQuitGroup = (groupID) => r({
    url: "/group-member", method: "DELETE",
    data : {groupID}
})

// group-joined-num 查询自己加入的小组的数
export const reqGroupJoinNum = () => r({
    url: "/group-joined-num", method: "GET"
})

// group-joined 查询自己加入的小组功能
export const reqGroupJoined = (start, end) => r({
    url: "/group-joined", method: "GET",
    params : {start, end}
})


// homework 添加作业
export const reqAddHomework = (name, type, desc, subject, endtime, canSubmitAfterEnd, groupID) => r({
    url: "/homework", method: "POST",
    data: {name, type, desc, subject, endtime, canSubmitAfterEnd, groupID}
})

// homework 查询作业
export const reqQueryHomework = (homeworkID) => r({
    url: "/homework", method: "GET",
    params: {homeworkID}
})

// homework 删除作业
export const reqDelHomework = (homeworkID) => r({
    url: "/homework", method: "DELETE",
    data : {homeworkID}
})

// 修改作业
export const reqAdjustHomework = (homeworkID, name, type, desc, subject, endtime, canSubmitAfterEnd, groupID) => r({
    url: "/homework?homeworkID=" + homeworkID, method: "PUT",
    data: {name, type, desc, subject, endtime, canSubmitAfterEnd, groupID}
})

// homework-created-num 查询自己创建的作业的数量（用于分页等）
export const reqHomeworkCreatedNum = () => r({
    url: "homework-created-num", method: "GET"
})


// homework-created 查询自己创建的作业
export const reqHomeworkCreated = (start, end) => r({
    url: "/homework-created", method: "GET",
    params: {start, end}
})

// submissions 查询单个作业的完成情况
export const reqSubmissions = (homeworkID) => r({
    url: "/submissions", method: "GET",
    params: {homeworkID}
})

// submission-file 下载提交的文件
export const reqGetFileSubmissionID = (submissionID) => r({
    url: "/submission-file", method: "GET",
    params: {submissionID}
})

export const reqGetFileHomeworkID = (homeworkID) => r({
    url: "/submission-file", method: "GET",
    params: {homeworkID}
})

// homework-joined-num
export const reqHomeworkJoinedNum = () => r({
    url: "/homework-joined-num", method: "GET"
})

// homework-joined
export const reqHomeworkJoined = (start, end) => r({
    url: "/homework-joined", method: "GET",
    params: {start, end}
})

// 没有完成的作业的数量 homework-not-finished-num
export const reqHomeworkNotFinishedNum = () => r({
    url: "/homework-not-finished-num", method: "GET"
})

// 没完成的作业 homework-not-finished
export const reqHomeworkNotFinished = (start, end) => r({
    url: "/homework-not-finished", method: "GET",
    params: {start, end}
})


// 提交作业 submit
export const reqSubmitHomework = (homeworkID, file) => r({
    url: "/submit?homeworkID=" + homeworkID, method: "POST",
    data : {file} 
})

// 一次性打包所有文件
export const reqExportAll = (homeworkID) => r({
    url: "/export", method: "GET",
    params: {homeworkID}
})

// 配置
export const reqGetConfig = () => r({
    url: "/config", method: "GET",
})

export const reqUpdateConfig = (config) => r({
    url: "/config", method: "PUT",
    data: {config}
})