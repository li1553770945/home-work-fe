import axios from "axios";
import nprogress from "nprogress";

// 基于api 的接口定义
const requests = axios.create({
    baseURL : "/api",
    timeout: 5000
})

// 使用 nprogress进行装饰
// 请求拦截器
requests.interceptors.request.use(
    (config) => {
        nprogress.start()
        return config
    }
)

// 响应拦截器
requests.interceptors.response.use(
    (res)   => {
        nprogress.done()
        return res.data;
    },
    (errot) => {
        return Promise.reject(new Error("fail"));
    }
)

export default requests;