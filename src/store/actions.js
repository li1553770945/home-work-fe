import { reqLogin, reqEnroll, reqLogout, reqQueryInfo } from "@/api"
import { reqAddGroup, reqDelGroup, reqAdjustGroup, reqQueryGroup, reqGroupCreateNum, reqGroupCreated, reqJoinGroup, reqQuitGroup, reqGroupJoinNum, reqGroupJoined } from "@/api"
import { reqAddHomework, reqDelHomework, reqQueryHomework, reqAdjustHomework, reqHomeworkCreatedNum, reqHomeworkCreated, reqHomeworkJoinedNum, reqHomeworkJoined, reqHomeworkNotFinished, reqHomeworkNotFinishedNum } from "@/api"
import { reqSubmissions, reqGetFileHomeworkID, reqGetFileSubmissionID, reqSubmitHomework } from "@/api"

import { reqExportAll, reqGetConfig, reqUpdateConfig } from "@/api"

const actions = {
    // 登录注册等
    async login({commit}, params) {
        let res = await reqLogin(params.username, params.password)
        if (res.code == 0)   commit("LOGIN", res.data)
        else                 commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async enroll({commit}, params) {
        let res = await reqEnroll(params.username, params.password, params.name)
        console.log(res)
        if (res.code == 0)  commit("ENROLL", res.data)
        else                 commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async logout({commit}, params) {
        let res = await reqLogout()
        if (res.code == 0)  commit("LOGOUT", res.data)
        else                 commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async queryInfo({commit}, params) {
        // 这个方法会在每次App挂载后发起，避免用户重复登陆
        let res = await reqQueryInfo()
        if (res.code == 0)  commit("LOGIN", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },

    // group类的方法
    async addGroup({commit}, params) {
        let res = await reqAddGroup(params.name, params.password, params.desc, params.allowCreate)
        if (res.code == 0)  commit("ADDGROUP", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async delGroup({commit}, params) {
        let res = await reqDelGroup(params.groupID)
        if (res.code == 0)  commit("DELGROUP", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async queryGroup({commit}, params) {
        let res = await reqQueryGroup(params.groupID)
        if (res.code == 0)  commit("QUERYGROUP", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async adjustGroup({commit}, params) {
        let res = await reqAdjustGroup(params.groupID, params.name, params.password, params.desc, params.allowCreate)
        if (res.code == 0)  commit("ADJUSTGROUP", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async groupCreateNum({commit}, params) {
        let res = await reqGroupCreateNum()
        if (res.code == 0)  commit("GROUPCREATENUM", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async groupCreated({commit}, params) {
        let res = await reqGroupCreated(params.start, params.end)
        if (res.code == 0)  commit("GROUPCREATED", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async joinGroup({commit}, params) {
        let res = await reqJoinGroup(params.groupID, params.password)
        if (res.code == 0)  commit("JOINGROUP", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async quitGroup({commit}, params) {
        let res = await reqQuitGroup(params.groupID)
        if (res.code == 0)  commit("QUITGROUP", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async groupJoinNum({commit}, params) {
        let res = await reqGroupJoinNum()
        if (res.code == 0)  commit("GROUPJOINNUM", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async groupJoined({commit}, params) {
        let res = await reqGroupJoined(params.start, params.end)
        if (res.code == 0)  commit("GROUPJOINED", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },


    // homework
    async addHomework({commit}, params) {
        let res = await reqAddHomework(params.name, params.type, params.desc, params.subject, params.endtime, params.canSubmitAfterEnd, params.groupID)
        if (res.code == 0)  commit("ADDHOMEWORK", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async delHomework({commit}, params) {
        let res = await reqDelHomework(params.homeworkID)
        if (res.code == 0)  commit("DELHOMEWORK", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async queryHomework({commit}, params) {
        let res = await reqQueryHomework(params.homeworkID)
        if (res.code == 0)  commit("QUERYHOMEWORK", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async adjustHomework({commit}, params) {
        let res = await reqAdjustHomework(params.homeworkID, params.name, params.type, params.desc, params.subject, params.endtime, params.canSubmitAfterEnd, params.groupID)
        if (res.code == 0)  commit("ADJUSTHOMEWORK", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async homeworkCreatedNum({commit}, params) {
        let res = await reqHomeworkCreatedNum()
        if (res.code == 0)  commit("HOMEWORKCREATEDNUM", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async homeworkCreated({commit}, params) {
        let res = await reqHomeworkCreated(params.start, params.end)
        if (res.code == 0)  commit("HOMEWORKCREATED", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async homeworkJoinedNum({commit}, params) {
        let res = await reqHomeworkJoinedNum()
        if (res.code == 0)  commit("HOMEWORKJOINEDNUM", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async homeworkJoined({commit}, params) {
        let res = await reqHomeworkJoined(params.start, params.end)
        if (res.code == 0)  commit("HOMEWORKJOINED", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async homeworkNotFinishedNum({commit}, params) {
        let res = await reqHomeworkNotFinishedNum()
        if (res.code == 0)  commit("HOMEWORKNOTFINISHEDNUM", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async homeworkNotFinished({commit}, params) {
        let res = await reqHomeworkNotFinished(params.start, params.end)
        if (res.code == 0)  commit("HOMEWORKNOTFINISHED", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async submissions({commit}, params) {
        console.log(params)
        let res = await reqSubmissions(params.homeworkID)
        if (res.code == 0)  commit("SUBMISSIONS", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async getFileHomeworkID({commit}, params) {
        let res = await reqGetFileHomeworkID(params.homeworkID)
        if (res.code == 0)  commit("GETFILEHOMEWORKID", res.data.token)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async getFileSubmissionID({commit}, params) {
        let res = await reqGetFileSubmissionID(params.submissionID)
        if (res.code == 0)  commit("GETFILESUBMISSIONID", res.data.token)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async submitHomework({commit}, params) {
        let res = await reqSubmitHomework(params.homeworkID, params.file)
        if (res.code == 0)  commit("SUBMITHOMEWORK", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },

    // 打包所有文件
    async exportAll({commit}, params) {
        let res = await reqExportAll(params.homeworkID)
        if (res.code == 0)  commit("EXPORTALL", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async getConfig({commit}, params) {
        let res = await reqGetConfig()
        if (res.code == 0)  commit("GETCONFIG", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    },
    async updateConfig({commit}, params) {
        let res = await reqUpdateConfig(params.config)
        if (res.code == 0)  commit("UPDATECONFIG", res.data)
        else                commit("ERROR", {error: res.msg, errorcode: res.code})
    }
}

export default actions