const mutations = {
    CHANGEGOLOBALCOLOR(state, payload) {
        if (payload.rgba != undefined)
            state.curGlobalColor = payload.rgba
    },
    CHANGELOGINSTATE(state, payload) {
        state.hasLogin = payload.hasLogin
    },
    ERROR(state, payload) {
        state.error = payload.error
        state.errorcode = payload.errorcode
    },
    LOGIN(state, payload) {
        state.login.id = payload.id
        state.login.username = payload.username
        state.login.name = payload.name
        state.login.status = payload.status
        state.login.created_at = payload.created_at
        state.login.last_login = payload.last_login
        state.hasLogin = true
    },
    ENROLL(state, payload) {
        state.opSuccess = true
    },
    LOGOUT(state, payload) {
        state.hasLogin = false
    },
    // 团队相关
    ADDGROUP(state, payload) {
        // 创建小组
        state.opSuccess = true
    },
    DELGROUP(state, payload) {
        // 删除小组
        state.opSuccess = true 
    },
    QUERYGROUP(state, payload) {
        // 查询小组的详细信息
        state.group.groupQuery = payload
        state.opSuccess = true
    },
    ADJUSTGROUP(state, payload) {
        // 修改小组信息
        state.opSuccess = true
    },
    GROUPCREATENUM(state, payload) {
        // 我创建的小组的数量
        state.group.groupCreateNum = payload
    },
    GROUPCREATED(state, payload) {
        // 我创建的小组，一次最多查询100条
        state.group.groupCreated = payload
    },
    JOINGROUP(state, payload) {
        state.opSuccess = true
    },
    QUITGROUP(state, payload) {
        state.opSuccess = true
    },
    GROUPJOINNUM(state, payload) {
        state.group.joinedGroupNum = payload
    },
    GROUPJOINED(state, payload) {
        state.group.joinedGroups = payload
    },

    // homework
    ADDHOMEWORK(state, payload) {
        state.opSuccess = true
    },
    DELHOMEWORK(state, payload) {
        state.opSuccess = true
    },
    QUERYHOMEWORK(state, payload) {
        state.homework.homeworkQuery = payload
        state.opSuccess = true
    },
    ADJUSTHOMEWORK(state, payload) {
        state.opSuccess = true
    },
    HOMEWORKCREATEDNUM(state, payload) {
        state.homework.homeworkCreatedNum = payload
    },
    HOMEWORKCREATED(state, payload) {
        state.homework.homeworkCreated = payload
    },
    HOMEWORKJOINEDNUM(state, payload) {
        state.homework.joinedHomeworkNum = payload
    },
    HOMEWORKJOINED(state, payload) {
        state.homework.joinedHomework = payload
    },
    HOMEWORKNOTFINISHEDNUM(state, payload) {
        // 未完成的作业数量
        state.homework.joinedHomeworkNotFinishedNum = payload
    },
    HOMEWORKNOTFINISHED(state, payload) {
        state.homework.joinedHomeworkNotFinished = payload
    },
    // 查看提交信息与下载
    SUBMISSIONS(state, payload) {
        state.homework.submissions = payload
        state.opSuccess = true
    },
    GETFILEHOMEWORKID(state, payload) {
        state.download.token = payload
        let url = "https://" + window.location.host + "/api/download?token=" + payload
        window.open(url)
    },
    GETFILESUBMISSIONID(state, payload) {
        state.download.token = payload
        let url = "https://" + window.location.host + "/api/download?token=" + payload
        window.open(url)
    },
    // 提交作业
    SUBMITHOMEWORK(state, payload) {
        state.opSuccess = true
    },
    EXPORTALL(state, payload) {
        state.export.done = payload.done
        if (payload.done)
            state.export.token = payload.token
    },
    GETCONFIG(state, payload) {
        if (payload.config == "") {

        }
        else {
            let config = JSON.parse(payload.config)
            state.curGlobalColor = config.curGlobalColor || state.curGlobalColor
            state.backgroundImg = config.backgroundImg || state.backgroundImg
            state.opacity = config.opacity || state.opacity
            state.textAreaMove = config.textAreaMove || state.textAreaMove
        }
    },
    UPDATECONFIG(state, payload) {
        state.opSuccess = true
    }
}   

export default mutations