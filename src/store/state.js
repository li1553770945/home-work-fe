const state = {
    curGlobalColor : "rgba(134, 21, 199, 1)",
    backgroundImg : 'https://cdn.jsdelivr.net/gh/LSTM-Kirigaya/KImage/Img/2k.jpg',
    opacity : 0.65,
    textAreaMove : true,
    isRouterAlive : true,
    hasLogin: false,
    error : undefined,
    errorcode : undefined,
    // 用于指示全局当前异步操作是否成功
    opSuccess : undefined,
    // login
    login : {
        id : undefined,
        username : undefined,
        name : undefined,
        status : undefined,
        created_at : undefined,
        last_login : undefined
    },
    group : {
        groupCreateNum : undefined,
        groupCreated   : [],
        groupQuery : {
            id : undefined,
            password : "",
            name : "",
            desc : "",
            CreatedAt : "",
            owner : {name : "", id: ""},
            members : [],
            allowCreate : undefined
        },
        joinedGroupNum : undefined,
        joinedGroups : []
    },
    homework : {
        homeworkCreatedNum : undefined,
        homeworkCreated : [],
        submissions : [],
        homeworkQuery : {
            id : undefined,
            name : "",
            desc: "",
            subject: "",
            CreatedAt: "",
            endtime: "",
            CanSubmitAfterEnd : false,
            GroupID: undefined,
            type : "",
            owner: { id: undefined, name: "" }
        },
        joinedHomeworkNum : undefined,
        joinedHomework : [],
        joinedHomeworkNotFinishedNum : undefined,
        joinedHomeworkNotFinished : [],
    },
    download : {
        token : undefined
    },  
    export : {
        token : undefined,
        done : false,
        queryTime : 0
    }
}

export default state