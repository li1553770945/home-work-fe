import { ElNotification } from 'element-plus'

export default function hello() {
    let date = new Date()
    let hour = date.getHours()
    let minute = date.getMinutes()
    let title = ""
    let message = ""
    if (hour >= 0 && hour <= 5) {
      title = "凌晨好"
      message = "注意身体，少熬夜"
    }
    else if (hour <= 11) {
      title = "早上好"
      message = "早上或许应该来一杯咖啡☕？啊，红茶也不错。"
    }
    else if (hour <= 13) {
      title = "中午好"
      message = "中午的闲暇时光可不能浪费了呢"
    }
    else if (hour <= 18) {
      title = "下午好"
      message = "我喜欢在下午来一杯红茶再加上一点点甜甜的点心"
    }
    else {
      title = "晚上好"
      message = "夜幕已至，夜猫子准备干活了😼"
    }

    ElNotification({
      title: title,
      message: message,
      'z-index': 201
    })
  }