// @ts-check

/** 
 * rgb 转 十六进制
 * @param { Array } rgb
 * @returns { string }
*/
export function rgb2hex(rgb) {
    if (rgb.length != 3)
        return ""
    let hex = "#"
    let color
    for (let val of rgb) {
        color = val.toString(16)
        if (color.length == 1)
            color = '0' + color
        hex += color
    }
    return hex
}

/** 
 * 十六进制 转 rgb
 * @param { string } hex
 * @returns { Array }
*/
export function hex2rgb(hex) {
    if (hex[0] != '#')
        hex = '#' + hex
    if (hex.length != 7)
        return []
    hex = hex.toLowerCase()
    let rgb = [
        parseInt('0x' + hex.substring(1, 3)),
        parseInt('0x' + hex.substring(3, 5)),
        parseInt('0x' + hex.substring(5, 7))
    ]
    return rgb
}

/** 
 * rgb 转 hsv
 * @param { Array } rgb
 * @returns { Array }
*/
export function rgb2hsv(rgb) {
    if (rgb.length != 3)
        return []
    let max, min, h, s, v
    max = Math.max(...rgb)
    min = Math.min(...rgb)
    // s 和 v 是最好算的
    s = (max - min) / max;
    v = max;
    if (max == min)         h = 0;
    else if (rgb[0] == max) h = (rgb[1] - rgb[2]) / (max - min) * 60;
    else if (rgb[1] == max) h = (rgb[2] - rgb[0]) / (max - min) * 60 + 120;
    else if (rgb[2] == max) h = (rgb[0] - rgb[1]) / (max - min) * 60 + 240;
    if (h < 0)  h += 360;
    
    return [h, s, v]
}

/** 
 * hsv 转 rgb
 * @param { Array } hsv
 * @returns { Array }
*/
export function hsv2rgb(hsv) {
    if (hsv.length != 3)
        return []
    if (hsv[1] == 0) {
        let v = Math.round(hsv[2])
        return [v, v, v]
    }
        
    let h, s, v, a, b, c, f, i
    h = hsv[0]
    s = hsv[1]
    v = hsv[2]
    h /= 60
    i = Math.floor(h)
    f = h - i
    a = Math.round(v * (1 - s)) 
    b = Math.round(v * (1 - s * f))
    c = Math.round(v * (1 - s * (1 - f)))
    v = Math.round(v)
    switch (i) {
        case 0 : return [v, c, a];
        case 1 : return [b, v, a];
        case 2 : return [a, v, c];
        case 3 : return [a, b, v];
        case 4 : return [c, a, v];
        case 5 : return [v, a, b];
        case 6 : return [v, c, a];
    }
}

/** 
 * 返回 colormap 
 * @param { Array } rgb 参考颜色，会选取改颜色的色相计算colormap
 * @param { Number } n  返回的colormap的数量
 * @param { Number } lightness  亮度，其实就是V值
 * @returns { Array } rgb数组
*/
export function get_colormap(rgb, n, lightness=100) {
    if (rgb.length != 3 || n == 1)
        return []
    let hsv = rgb2hsv(rgb)
    let h = hsv[0]
    // 选定h 和 v不变，均匀选取s
    let mapping = [], seg = 1 / (n - 1)
    for (let i = 0; i < n; ++ i) {
        mapping.push(hsv2rgb([h, i * seg, lightness]))
    }
    return mapping
}

/** 
 * 返回 colormap 
 * @param { string } rgba rgba颜色，有固定的形式，如"rgba(245, 106, 106, 1)"
 * @returns { Array } rgb数组
*/
export function parseRGBA(rgba) {
    let rgb = [];
    let buff = "";
    for (let i = 0; i < rgba.length; ++ i) {
        if (rgba[i] >= '0' && rgba[i] <= '9') {
            buff += rgba[i]
        }
        else {
            if (buff.length > 0) {
                rgb.push(parseInt(buff))
                buff = ""
            }
        }
    }
    if (buff.length > 0)
        rgb.push(parseInt(buff))

    return [rgb[0], rgb[1], rgb[2]]
}

// let rgb = [255, 180, 2]
// let res = get_colormap(rgb, 10, 100)
// console.log(res)
// let hex = rgb2hex(rgb)
// let hsv = rgb2hsv(rgb)
// console.log(hex)
// console.log(hsv)
// console.log(hex2rgb(hex))
// console.log(hsv2rgb(hsv))
