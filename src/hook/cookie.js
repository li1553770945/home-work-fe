export function toBool(x) {
    return (x == 'true') ? true : false;
}

export function get_cookie() {
    if (document.cookie !== undefined) {
        let cookies = document.cookie.split(';')
        if (cookies.length > 1) {
            let cookie_map = {}, res;
            for (let index in cookies) {
                res = cookies[index].split('=')
                if (res.length == 1) {
                    continue
                }
                else {
                    cookie_map[res[0].trim()] = res[1]
                }
            }
            return cookie_map
        } 
    }
    return {}
}

export function set_cookie(key, value) {
    if (typeof(key) != "string") {
        alert("cookie key must be a string!")
        return false
    }
    document.cookie = key + "=" + value
    return true;
}

export function del_cookie(key) {
    if (typeof(key) != "string") {
        alert("cookie key must be a string!")
        return false
    }
    document.cookie = key + "=; " + "expires=Thu, 01 Jan 1970 00:00:00 GMT";
    return true;
}

export function show_cookie() {
    let cookies = get_cookie()
    console.log(cookies)
}