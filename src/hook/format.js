// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
export function formatTime(date, fmt) {
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "H+": date.getHours(), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

export function makePrettyTime(timeString) {
    let keyTime = timeString.split(/[T.]/g);
    let date = keyTime[0].split("-");
    let time = keyTime[1];
    let res =  `${date[0]}年${date[1]}月${date[2]}日 ${time}`;
    if (res.includes("+"))
        return res.split("+")[0]
    else 
        return res
}

function RFC3399toDate(RFC3399) {
    // RFC3399标准的字符串
    let d = RFC3399.substring(0, 19).replace(/-/g, "/").replace('T', ' ')
    d = new Date(d)
    return d
}

export function timeprogress(start, end) {
    // 输入的都是RFC3399标准的字符串
    start = RFC3399toDate(start)
    end = RFC3399toDate(end)


    let base = 86400000
    let cur = new Date(Date.now())
    let left_days = Math.floor((end - cur) / base * 100) / 100
    let timeprecent = {}

    if (cur >= end) {
        timeprecent.percent = 0
        timeprecent.caption = "已经截止了"
    }
    else {
        timeprecent.percent = Math.floor((end - cur) / (end - start) * 100)
        timeprecent.caption = "还剩" + left_days + "天"
    }
    
    return timeprecent
}